## Build

Install tools:

```
apt-get install build-essential linux-headers-`uname -r`
```

Then run `make`.

## Test module

Install:

```
sudo insmod lkm_example.ko
sudo dmesg
```

Remove:

```
sudo rmmod lkm_example
```
