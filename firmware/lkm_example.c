#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Robert W. Oliver II");
MODULE_DESCRIPTION("A simple example Linux module.");
MODULE_VERSION("0.01");

#define TEST_ADDR 0xc0000

static int __init lkm_example_init(void) {
 int* test;
 // int i;
 
 printk(KERN_INFO "-----------------------------------\n");
 printk(KERN_INFO "|     Firmware overwrite test     |\n");
 printk(KERN_INFO "-----------------------------------\n");
 printk(KERN_INFO "Trying to touch physical memory %#x\n", TEST_ADDR);
 test = phys_to_virt(TEST_ADDR);
 printk(KERN_INFO "Get virtual address: %#lx\n", (uintptr_t)test);
 printk(KERN_INFO "read value: %#x\n", *test);
 printk(KERN_INFO "write 0xff\n");
 *test = 0xff;
 printk(KERN_INFO "read value after modification: %#x\n", *test);

 /*for(i = 0; i < 4000; i++) {
     test += 1;
     if (*test != 0) {
       printk(KERN_INFO "memory %lx - %x\n", (uintptr_t)test, *test);
     }
 } */
 return 0;
}
static void __exit lkm_example_exit(void) {
 printk(KERN_INFO "Goodbye, World!\n");
}

module_init(lkm_example_init);
module_exit(lkm_example_exit);
