#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ioport.h>
#include <linux/kobject.h>
#include <linux/unistd.h>
#include <linux/syscalls.h>
#include <linux/string.h>
#include <linux/slab.h>

#include <asm/io.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ning Yang");
MODULE_DESCRIPTION("A simple linux module to dump kernel memory.");
MODULE_VERSION("0.01");

#define KERNEL_CODE								"Kernel code"
#define KERNEL_DATA								"Kernel data"
#define KERNEL_BSS								"Kernel bss"
#define SYSTEM_RAM								"System RAM"

#define QEMU_VM_EVENT_PORT 0x504
#define QEMU_VM_EVENT_REPORT_KERNEL 8

#define __RESOURCE_FOUND_BY_NAME(res, n) (res && strcmp(res->name, n) == 0)

static int get_resource_by_name(const char *name, struct resource **resource)
{
  int found = -1;

  struct resource *entry, *child;

  entry = iomem_resource.child;

  while (1) {
    if (entry == NULL)
      break;

    if (__RESOURCE_FOUND_BY_NAME(entry, SYSTEM_RAM)) {
      child = entry->child;

      while (1) {
        if (child == NULL)
          break;

        pr_err("<vbs>: Found kernel code region: %s start = 0x%llx, end = 0x%llx.\n",
          child->name, child->start, child->end);
        if (__RESOURCE_FOUND_BY_NAME(child, name)) {
          // pr_err("<vbs>: Found kernel code region: %s start = 0x%llx, end = 0x%llx.\n",
          //   child->name, child->start, child->end);
          *resource = child;
          found = 1;
          // break;
        }

        child = child->sibling;
      }
    }

    entry = entry->sibling;
  }

  return found;
}

#if defined(__i386__)
#define START_CHECK 0xc0000000
#define END_CHECK 0xd0000000
typedef unsigned int psize;
#else
#define START_CHECK 0xffffffff81000000
#define END_CHECK 0xffffffffa2000000
typedef unsigned long psize;
#endif

psize *sys_call_table;
psize **find(void) {
 psize **sctable;
 psize i = START_CHECK;
 while (i < END_CHECK) {
  sctable = (psize **) i;
  if (sctable[__NR_close] == (psize *) sys_close) {
   return &sctable[0];
  }
  i += sizeof(void *);
 }
 return NULL;
}

static void protect_pages(uint64_t start, uint64_t end) {
  uint32_t start_high, start_low, end_high, end_low;

  if (!inl(QEMU_VM_EVENT_PORT)) {
    pr_err("<vbs>: open port failed!\n");
    return;
  }
  outl(QEMU_VM_EVENT_REPORT_KERNEL, QEMU_VM_EVENT_PORT);
  start_high = start >> 32;
  start_low = start;
  end_high = end >> 32;
  end_low = end;
  outl(start_high, QEMU_VM_EVENT_PORT);
  outl(start_low, QEMU_VM_EVENT_PORT);
  outl(end_high, QEMU_VM_EVENT_PORT);
  outl(end_low, QEMU_VM_EVENT_PORT);
}


static int __init kernel_memory_dump_init(void) {
  struct resource* target;
  int num_page, total_size;
  uint64_t phys_addr, page_table_start, page_table_end;

  printk(KERN_INFO "kernel_memory_dump_init!\n");
  if (get_resource_by_name(KERNEL_CODE, &target) != 0) {
    total_size = target->end - target->start;
    num_page = total_size / 0x400;
    pr_err("<vbs>: Found %s region, total_size %#x, num_of_page: %d.\n", 
      target->name, total_size, num_page);
  }

  if ((sys_call_table = (psize *) find())) {
    printk("<vbs>: sys_call_table found at %#lx\n", (uintptr_t)sys_call_table);
    phys_addr = virt_to_phys(sys_call_table);
    printk("<vbs>: phys address at %#lx\n", (uintptr_t)phys_addr);
    page_table_start = (phys_addr / PAGE_SIZE) * PAGE_SIZE;
    page_table_end = page_table_start + PAGE_SIZE;
    printk("<vbs>: try to protect [%#llx, %#llx]\n", page_table_start, page_table_end);
  }

  // protect_pages(target->start, target->end);
  
  return 0;
}

static void __exit kernel_memory_dump_exit(void) {
  printk(KERN_INFO "kernel_memory_dump_exit!\n");

}


module_init(kernel_memory_dump_init);
module_exit(kernel_memory_dump_exit);
